#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Science                         ##
## Filename: ./legacy/1-maria-ingest-science.sh             ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f libgen.rar

# Fetch the science database from your favorite mirror
wget -c http://185.39.10.101/dbdumps/libgen/libgen_all_dbbackup-last.rar -O libgen.rar
#wget -c http://libgen.lc/dbdumps/libgen/libgen_all_dbbackup-last.rar -O libgen.rar
#wget -c http://gen.lib.rus.ec/dbdumps/libgen.rar

# Unpack the database
unrar x libgen.rar
rm -f libgen.rar

# Some archives extract as backup_libgen_all.sql
# we will attempt to rename to libgen.sql, just in case
mv backup_libgen_all.sql libgen.sql

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' libgen.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen;"
sudo mysql -e "CREATE DATABASE libgen;"

# Import our databse into MariaDB
sudo mysql libgen < libgen.sql
rm -f libgen.sql

# Change torrent hashes from text to VARCHAR(500)
sudo mysql -e "ALTER TABLE libgen.hashes CHANGE torrent torrent VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;"

# Edit topic structure to be compatible with flat JSON export
# Add column for topics in Russian
sudo mysql -e "ALTER TABLE libgen.topics ADD topic_descr_rus VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER topic_descr;"

# Place Russian topics in their new column and remove entries from original column
sudo mysql -e "UPDATE libgen.topics SET topic_descr_rus = topic_descr WHERE lang='ru';"
sudo mysql -e "UPDATE libgen.topics SET topic_descr = '' WHERE lang='ru';"

# Create a new topics table that with a flat format, set topic_descr to VARCHAR(500) and add topic_id as index
sudo mysql -e "CREATE TABLE libgen.topics_edited AS SELECT id as id, topic_id,GROUP_CONCAT(topic_descr SEPARATOR '') AS topic_descr, topic_descr_rus AS topic_descr_rus, kolxoz_code as kolxoz_code, topic_id_hl as topic_id_hl FROM libgen.topics GROUP BY topic_id;"
sudo mysql -e "ALTER TABLE libgen.topics_edited CHANGE topic_descr topic_descr VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;"
sudo mysql -e "ALTER TABLE libgen.topics_edited ADD INDEX(topic_id);"

# Download IPFS hashes database, create ipfs_hashes table, import the hashes and index the md5 column

wget -c https://ipfs.io/ipns/databases.libgen.eth/ipfs_science_hashes_2526.txt
mv ipfs_science_hashes_2526.txt ~/knowl/ingest/

sudo mysql -e "CREATE TABLE libgen.ipfs_hashes (ipfs_blake2b CHAR(62) CHARACTER SET utf8 NOT NULL, md5 CHAR(32) CHARACTER SET utf8 NOT NULL) CHARACTER SET utf8 ENGINE=MyISAM COLLATE=utf8_general_ci;"
sudo mysql -e "LOAD DATA LOCAL INFILE '/home/bookuser/knowl/ingest/ipfs_science_hashes_2526.txt' INTO TABLE libgen.ipfs_hashes FIELDS TERMINATED BY ' ';"
sudo mysql -e "ALTER TABLE libgen.ipfs_hashes ADD INDEX(md5);"


# Next, you should call the json script
# sudo sh ../legacy/2-maria-to-json-science.sh

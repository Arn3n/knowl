#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Fiction                         ##
## Filename: ./legacy/1-maria-ingest-fiction.sh             ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f backup_libgen_fiction.rar

# Fetch the fiction database from your favorite mirror
#wget -c http://185.39.10.101/dbdumps/fiction/libgen_fiction_dbbackup-last.rar -O backup_libgen_fiction.rar
#wget -c http://libgen.lc/dbdumps/fiction/libgen_fiction_dbbackup-last.rar -O backup_libgen_fiction.rar
wget -c http://gen.lib.rus.ec/dbdumps/fiction.rar -O backup_libgen_fiction.rar

# Unpack the database
unrar x backup_libgen_fiction.rar
rm -f backup_libgen_fiction.rar

# Some archives extract as fiction.sql
# we will attempt to rename to backup_libgen_fiction.sql, just in case
mv fiction.sql backup_libgen_fiction.sql

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' backup_libgen_fiction.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen_fiction;"
sudo mysql -e "CREATE DATABASE libgen_fiction;"

# Import our databse into MariaDB
sudo mysql libgen_fiction < backup_libgen_fiction.sql
rm -f backup_libgen_fiction.sql

# Download IPFS hashes database, create ipfs_hashes table, import the hashes and index the md5 column

wget -c https://ipfs.io/ipns/databases.libgen.eth/ipfs_fiction_hashes_no_extensions.txt
mv ipfs_fiction_hashes_no_extensions.txt ~/knowl/ingest/

sudo mysql -e "CREATE TABLE libgen_fiction.ipfs_hashes (ipfs_blake2b CHAR(62) CHARACTER SET ascii NOT NULL, md5 CHAR(32) CHARACTER SET ascii NOT NULL) CHARACTER SET utf8 ENGINE=MyISAM COLLATE=utf8_general_ci;"
sudo mysql -e "LOAD DATA LOCAL INFILE '/home/bookuser/knowl/ingest/ipfs_fiction_hashes_no_extension.txt' INTO TABLE libgen_fiction.ipfs_hashes FIELDS TERMINATED BY ' ';"
sudo mysql -e "ALTER TABLE libgen_fiction.ipfs_hashes ADD INDEX(md5);"


# Next, you should call the json script
# sudo sh ../legacy/2-maria-to-json-fiction.sh

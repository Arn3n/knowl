<img src="logo512.png" width="250">

## Overview

The ***Knowl Bookshelf*** aims to be a unified front-end for all of your ebook databases.  Often, there may be significant structural differences between various ebook sources (i.e. Project Gutenberg Project, Internet Archive, Open Library, etc). ***Knowl Bookshelf*** mutates these differing sources into a common set of univeral Knowl fields. This consolidated/unified database is then presented to the end user as an elegant Single-Page Application (SPA). 

If you are looking for books (pdf, epub, etc) you are in the wrong place.  This page in only for the organization and retrieval of your existing book lists.  If you want to download some free books, I highly recommend you visit https://www.gutenberg.org/.  However, you do not need to own a single eBook to work through this project.

## Objective

Setup a search engine for eBooks.  Search technology will be based on Elasticsearch, Logstash, and Kibana (collectively referred to as the “ELK Stack”).  We also have some advanced feautes planned on our <a href="https://gitlab.com/lucidhack/knowl/-/wikis/Roadmap-Features">***Features Roadmap***</a> page.    

We will be using Libgen book listings as our example data. However, with some modification, these techniques should also work for other publicly available book lists.  The reader is encouraged to expand these techniques to additional datasets, and share their logstash *.config files (submit a merge request for your *.config to the conf.d folder

## Screenshots

Detail View (default): 

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/250e4fe95657baff6c6006ba25c1347b/detail-view.png" width="800">

Bookshelf View:

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/e5a99b3d8c087759d609bca0ccc2ed02/grid-view.png" width="800">

## Procedural Book-Cover Generation

If a book-cover does not exist on disk, one will be generated on the fly. There are 256 different texture backgrounds, and various text and decoration styles.  Combined, this allows for a wide range of book-styles that will be automagically applied to books with no covers. A book-style (texture and label) is each determined at run-time by characters in the MD5 hash.  This ensures that the same book-style cover is applied across different systems.  

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/0641b1ad227c677377d17b99b4ad62e8/random-cover-generator.png" width="800">

## Getting Started

Proceed to the <a href="https://gitlab.com/lucidhack/knowl/-/wikis/Getting-Started"><b>Getting Started</b></a> Wiki page to get ***Knowl Bookshelf*** up and running on your own system.

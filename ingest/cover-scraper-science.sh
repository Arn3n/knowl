# Download the first 100 bookcovers from the libgen-science collection
# Modify or delete "AND id < 100" to get more records,
# or change to "AND id > 1000000" to start at a last known point (ie 1M)

echo "Consolidating covers from last run..."
sudo rsync -a -v --stats --progress --remove-source-files libgen.lc/covers/ ../bookshelf/public/covers/Science/
sudo rm -r libgen.lc/covers/*

echo "Downloading new covers..."
sudo mysql -e "SELECT coverurl FROM libgen.updated WHERE coverurl != '' AND id < 100 ORDER BY id" | while read coverurl; do
  if [ ! -f ../bookshelf/public/covers/Science/$coverurl ]; then
    echo $coverurl " missing on disk.  downloading..."
    wget -c --force-directories "https://libgen.lc/covers/"$coverurl
  else
    echo $coverurl " exists  on disk.  skipping download."
  fi
done

echo "Consolidating covers from this run..."
sudo rsync -a -v --stats --progress --remove-source-files libgen.lc/covers/ ../bookshelf/public/covers/Science/
sudo rm -r libgen.lc/covers/*
